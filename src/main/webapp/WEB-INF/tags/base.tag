<%--
  Created by IntelliJ IDEA.
  User: xelekgakure
  Date: 09/04/19
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">JavaWebApplication</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="#">Un Lien</a></li>
            <li><a href="#">Un Autre Lien</a></li>
            <li><a href="#">Pouleh</a></li>
        </ul>
    </div>
</nav>

<div id="pageheader">
    <jsp:invoke fragment="header"/>
</div>
<div id="body">
    <jsp:doBody/>
</div>
<div id="pagefooter">
    <jsp:invoke fragment="footer"/>
</div>
</body>
</html>