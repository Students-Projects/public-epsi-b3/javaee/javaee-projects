<%--
  Created by IntelliJ IDEA.
  User: xelekgakure
  Date: 09/04/19
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:base>
    <jsp:attribute name="header">
      <h1>Interface de Rendu JEE</h1>
    </jsp:attribute>
    <jsp:attribute name="footer">
      <p id="signature">Julien Lauret - Epsi B3 Groupe 2</p>
    </jsp:attribute>
    <jsp:body>
        <div class="container">
            <div class="row">
                <a href='<c:url value="/epsi/inscription"/>'>JEE Project - EPSI Inscription</a>
            </div>
        </div>
    </jsp:body>
</t:base>