<%--
  Created by IntelliJ IDEA.
  User: xelekgakure
  Date: 10/04/19
  Time: 09:52
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:base>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="footer">
      <p id="signature">Julien Lauret - Epsi B3 Groupe 2</p>
    </jsp:attribute>
    <jsp:body>
        <div class="container" style="padding: 5%">
            <div class="row">
                <form action="./epsi/inscription" method="post" accept-charset="UTF-8">
                    <label for="nom">Nom</label>
                    <input id="nom" type="text" name="nom" placeholder="Nom" value="${param['nom']}" required>
                    <label for="prenom">Prénom</label>
                    <input id="prenom" type="text" name="prenom" placeholder="Prenom" value="${param['prenom']}" required>
                    <div class="input-field col s12">
                        <label for="year">Année</label>
                        <select id="year" name="year" required>
                            <option disabled selected>Choose Your Option...</option>
                            <c:forEach items="${annee}" var="i">
                                <option value="${i.getId()}">${i.getName()}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <button type="submit" class="button ">Submit</button>
                </form>
            </div>
        </div>
    </jsp:body>
</t:base>