package BakaNoLaboratory.Epsi;

import java.util.ArrayList;

public class Annees extends ArrayList<Annee> {

    private int id = 0;

    public boolean addAnnee(String name) {
        Annee a = new Annee(this.id++, name);
        return this.add(a);
    }

    public Annee getAnnee(int id) {
        return this.get(id);
    }

    public ArrayList<Annee> getAnnees() {
        return this;
    }




}
