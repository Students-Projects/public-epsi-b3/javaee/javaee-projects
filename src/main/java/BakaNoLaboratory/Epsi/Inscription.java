package BakaNoLaboratory.Epsi;

public class Inscription {

    private String nom;
    private String prenom;
    private Annee year;

    public Inscription(String nom, String prenom, Annee year)
    {
        this.nom = nom;
        this.prenom = prenom;
        this.year = year;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Annee getYear() {
        return year;
    }

    public void setYear(Annee year) {
        this.year = year;
    }
}
