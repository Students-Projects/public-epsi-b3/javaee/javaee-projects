package Service.Epsi;

import BakaNoLaboratory.Epsi.Annee;
import BakaNoLaboratory.Epsi.Inscription;
import BakaNoLaboratory.Exception.InscriptionInvalideException;

public class InscriptionService {
    public Inscription inscrire(String nom, String prenom, Annee year) throws InscriptionInvalideException {
        InscriptionInvalideException ex = new InscriptionInvalideException();

        if (nom == null) {
            ex.addMessage("nom", "Nom invalide !");
        }
        if (prenom == null) {
            ex.addMessage("prenom", "Prénom Invalide !");
        }
        if (year == null) {
            ex.addMessage("prenom", "Prénom Invalide !");
        }
        if (ex.mustBeThrown()) {
            throw ex;
        }

        return new Inscription(nom,prenom, year);
    }
}
