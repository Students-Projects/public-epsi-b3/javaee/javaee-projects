package Controller.Epsi;

import BakaNoLaboratory.Epsi.Annee;
import BakaNoLaboratory.Epsi.Annees;
import BakaNoLaboratory.Epsi.Inscription;
import BakaNoLaboratory.Exception.InscriptionInvalideException;
import Service.Epsi.InscriptionService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/epsi/inscription",name = "InscriptionController")
public class InscriptionController extends HttpServlet {

    private Annees a = new Annees();
    public InscriptionController()
    {
        this.a.addAnnee("Bachelor niveau 1");
        this.a.addAnnee("Bachelor niveau 2");
        this.a.addAnnee("Bachelor niveau 3");
        this.a.addAnnee("Ingénieur niveau 4");
        this.a.addAnnee("Ingénieur niveau 5");
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String y = request.getParameter("year");
        Annee year = this.a.getAnnee(1);
        try {
            InscriptionService inscriptionService = new InscriptionService();
            Inscription inscription = inscriptionService.inscrire(nom, prenom, year);
            request.setAttribute("inscription", inscription);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/epsi/validationInscription.jsp");
            rd.forward(request, response);
        } catch (InscriptionInvalideException e) {
            request.setAttribute("errors", e.getMessages());
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/epsi/inscription.jsp");
            rd.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("annee", this.a);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/WEB-INF/jsp/epsi/inscription.jsp");
        rd.forward(request, response);
    }
}
